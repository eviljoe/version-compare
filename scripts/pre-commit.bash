#!/bin/bash
# Run the pre-commit checks

set -euo pipefail

this_script="$(readlink -f "${BASH_SOURCE[0]}")"
this_script_dir="$(dirname "${this_script}")"
cd "${this_script_dir}/.."

scripts/lint.bash
echo
scripts/test.bash
