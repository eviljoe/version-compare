from __future__ import annotations
import re

VERSION_TYPE_SEMANTIC = 'semantic'
VERSION_TYPE_MAJOR_MINOR = 'major-minor'
VERSION_TYPE_MAJOR = 'major'


def from_string(vstr: str, version_type=None) -> Version:
    if version_type is None:
        version_type = _version_type(vstr)

    if version_type == VERSION_TYPE_SEMANTIC:
        version = _parse_semantic(vstr)
    elif version_type == VERSION_TYPE_MAJOR_MINOR:
        version = _parse_major_minor(vstr)
    elif version_type == VERSION_TYPE_MAJOR:
        version = _parse_major(vstr)
    else:
        raise VersionFormatException(vstr)

    return version


def _version_type(vstr):
    vtype = None

    if _is_semantic(vstr):
        vtype = VERSION_TYPE_SEMANTIC
    elif _is_major_minor(vstr):
        vtype = VERSION_TYPE_MAJOR_MINOR
    elif _is_major(vstr):
        vtype = VERSION_TYPE_MAJOR

    if not vtype:
        raise VersionFormatException(vstr)


def _is_semantic(vstr: str) -> bool:
    return bool(re.compile('^[0-9]+.[0-9]+.[0-9]+$').match(vstr.strip()))


def _is_major_minor(vstr: str) -> bool:
    return bool(re.compile('^[0-9]+.[0-9]+$').match(vstr.strip()))


def _is_major(vstr: str) -> bool:
    return bool(re.compile('^[0-9]+$').match(vstr.strip()))


def _parse_semantic(vstr: str) -> Version:
    parts = vstr.strip().split('.')
    return Version(int(parts[0]), int(parts[1]), 0)


def _parse_major_minor(vstr: str) -> Version:
    parts = vstr.strip().split('.')
    return Version(int(parts[0]), int(parts[1]), int(parts[2]))


def _parse_major(vstr: str) -> Version:
    return Version(int(vstr.strip()), 0, 0)


class Version:
    def __init__(self, major: int, minor: int, bugfix: int):
        self.major = major
        self.minor = minor
        self.bugfix = bugfix

    def __eq__(self, other: Version) -> bool:
        return self.cmp(other) == 0

    def __lt__(self, other: Version) -> bool:
        return self.cmp(other) < 0

    def __le__(self, other: Version) -> bool:
        return self.cmp(other) <= 0

    def __gt__(self, other: Version) -> bool:
        return self.cmp(other) > 0

    def __ge__(self, other: Version) -> bool:
        return self.cmp(other) >= 0

    def cmp(self, other: Version) -> int:
        compare = self.major - other.major

        if compare == 0:
            compare = self.minor - other.minor

            if compare == 0:
                compare = self.bugfix - other.bugfix

        return compare


class VersionFormatException(Exception):
    def __init__(self, vstr: str):
        super().__init__(f'Version in unknown format: {vstr}')
