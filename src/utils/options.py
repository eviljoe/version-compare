from dataclasses import dataclass


@dataclass
class VCompareOptions:
    comparison_type: str
    versions: [str]
    version_type: str
