from __future__ import annotations
from typing import Callable


class Comparison:
    def __init__(self, name: str, version_count: int, compare: Callable):
        self.name = name
        self.version_count = version_count
        self.compare = compare


class UnknownComparisonError:
    def __init__(self, comparison: str):
        super().__init__(f'Unknown comparison: {comparison}')


CMP_EQUAL = Comparison('Equal', 2, lambda v1, v2: v1 == v2)
CMP_LESS_THAN = Comparison('Less Than', 2, lambda v1, v2: v1 < v2)
CMP_LESS_THAN_EQUAL = Comparison('Less Than Equal To', 2, lambda v1, v2: v1 <= v2)
CMP_GREATER_THAN = Comparison('Greater Than', 2, lambda v1, v2: v1 > v2)
CMP_GREATER_THAN_EQUAL = Comparison('Greater Than Equal To', 2, lambda v1, v2: v1 >= v2)
CMP_BETWEEN_INCLUSIVE = Comparison('Between Inclusive', 3, lambda v1, v2, v3: v1 <= v2 <= v3)
CMP_BETWEEN_EXCLUSIVE = Comparison('Between Exclusive', 3, lambda v1, v2, v3: v1 < v2 < v3)

_COMPARISIONS = {
    CMP_EQUAL.name: CMP_EQUAL,
    CMP_LESS_THAN.name: CMP_LESS_THAN,
    CMP_LESS_THAN_EQUAL.name: CMP_LESS_THAN_EQUAL,
    CMP_GREATER_THAN.name: CMP_GREATER_THAN,
    CMP_GREATER_THAN_EQUAL.name: CMP_GREATER_THAN_EQUAL,
    CMP_BETWEEN_INCLUSIVE.name: CMP_BETWEEN_INCLUSIVE,
    CMP_BETWEEN_EXCLUSIVE.name: CMP_BETWEEN_EXCLUSIVE,
}


def compare_versions(comparison_name, v1, v2, v3=None) -> bool:
    comparison = _get_comparison(comparison_name)
    return comparison.compare(v1, v2) if comparison.version_count == 2 else comparison.compare(v1, v2, v3)


def version_count(comparison_name: str) -> int:
    return _get_comparison(comparison_name).version_count


def _get_comparison(comparison_name: str) -> Comparison:
    comparison = _COMPARISIONS[comparison_name]

    if not comparison:
        raise UnknownComparisonError(comparison_name)

    return comparison
