from mamba import after, description
from mockito import unstub

import vcompare

with description(vcompare):
    with after.each:
        unstub()
