#!/usr/bin/env python3

import argparse
import sys

from utils.options import VCompareOptions
from utils import version, compare

# ###########
# Constants #
# ###########


_NO_ERROR = 0
_ERROR_UNKNOWN = 1
_ERROR_COMPARISON_FALSE = 2
_ERROR_VALIDATION = 3


# #############
# Main Method #
# #############


def main():
    try:
        opts = _parse_args()
        _validate(opts)
        exit_code = _compare(opts)
    except ExitCodeError as e:
        exit_code = e.exit_code
        print(str(e), file=sys.stderr, flush=True)

    sys.exit(exit_code)


# ######################
# CLI Option Functions #
# ######################


def _parse_args() -> VCompareOptions:
    parser = argparse.ArgumentParser(description='Compare versions')

    # Version Type Options
    parser.add_argument('--semantic', '-s', action='store_const', const=version.VERSION_TYPE_SEMANTIC,
                        dest='version_type', help='Compare using #.#.# format')
    parser.add_argument('--major-minor', '-m', action='store_const', const=version.VERSION_TYPE_MAJOR_MINOR,
                        dest='version_type', help='Compare using #.# format')
    parser.add_argument('--major', '-M', action='store_const', const=version.VERSION_TYPE_MAJOR,
                        dest='version_type', help='Compare using # format')

    # Comparison Type Options
    parser.add_argument('--greater-than', '--gt', '-g', action='store_const', const=compare.CMP_GREATER_THAN.name,
                        dest='comparison_type', help='Check if v1 is greater than v2')
    parser.add_argument('--greater-than-eq', '--gte', '-G', action='store_const',
                        const=compare.CMP_GREATER_THAN_EQUAL.name,
                        dest='comparison_type', help='Check if v1 is greater than or equal to v2')
    parser.add_argument('--less-than', '--lt', '-l', action='store_const', const=compare.CMP_LESS_THAN.name,
                        dest='comparison_type', help='Check if v1 is less than v2')
    parser.add_argument('--less-than-eq', '--lte', '-L', action='store_const', const=compare.CMP_LESS_THAN_EQUAL.name,
                        dest='comparison_type', help='Check if v1 is less than or equal to v2')
    parser.add_argument('--equal', '--eq', '-e', action='store_const', const=compare.CMP_EQUAL.name,
                        dest='comparison_type', help='Check if v1 is equal to v2')
    parser.add_argument('--between-inclusive', '--bi', '-b', action='store_const',
                        const=compare.CMP_BETWEEN_INCLUSIVE.name,
                        dest='comparison_type', help='Check if v2 is between v1 and v3 (inclusive)')
    parser.add_argument('--between-exclusive', '--be', '-B', action='store_const',
                        const=compare.CMP_BETWEEN_EXCLUSIVE.name,
                        dest='comparison_type', help='Check if v2 is between v1 and v3 (exclusive)')

    # Positional Arguments
    parser.add_argument(nargs='+', action='store', metavar='version', dest='versions',
                        help='A version to be compared.  Depending on the type of comparison, two or three versions'
                             ' should be provided')

    return _to_options(parser.parse_args())


def _to_options(opts: argparse.Namespace) -> VCompareOptions:
    return VCompareOptions(
        comparison_type=opts.comparison_type,
        versions=opts.versions,
        version_type=opts.version_type,
    )


# ######################
# Validation Functions #
# ######################


def _validate(opts: VCompareOptions) -> None:
    _validate_version_count(opts)
    _validate_versions(opts)


def _validate_version_count(opts):
    expected = compare.version_count(opts.comparison_type)
    actual = len(opts.versions) if opts.versions else 0

    if expected != actual:
        raise InvalidVersionCountError(opts.comparison_type, expected, actual)


def _validate_versions(opts: VCompareOptions) -> None:
    for v in opts.versions:
        try:
            version.from_string(v)
        except version.VersionFormatException as e:
            raise ExitCodeError(str(e), _ERROR_VALIDATION) from e


# ######################
# Comparison Functions #
# ######################


def _compare(opts: VCompareOptions) -> int:
    v1 = version.from_string(opts.versions[0])
    v2 = version.from_string(opts.versions[1])
    v3 = version.from_string(opts.versions[2]) if len(opts.versions) >= 3 else None

    return _NO_ERROR if compare.compare_versions(opts.comparison_type, v1, v2, v3) else _ERROR_COMPARISON_FALSE


# ########
# Errors #
# ########


class ExitCodeError(Exception):
    def __init__(self, msg, exit_code=_ERROR_UNKNOWN):
        super().__init__(msg)
        self.exit_code = exit_code


class InvalidVersionCountError(ExitCodeError):
    def __init__(self, comparison_type: str, expected_count: int, actual_count: int):
        super().__init__('Incorrect number of versions given for comparison: '
                         f'comparison={comparison_type}, expected={expected_count}, actual={actual_count}',
                         exit_code=_ERROR_VALIDATION)


##############
# Main Check #
##############


if __name__ == '__main__':
    main()
